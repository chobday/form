import express from 'express'
import Cors from 'cors'
import bodyParser from 'body-parser'
import ApiRouter from './ApiRouter'

const app = express()

//TODO whitelist
app.use(Cors())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use('/api', ApiRouter)


const port = 3001
app.listen(port, () => console.log(`Listening on port ${port}!`))