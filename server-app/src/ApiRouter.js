import express from 'express'
import ddb from './Dynamo'

const router = express.Router()
export default router

router.use((req, res, next) => {
    const options = { year: '2-digit', month: '2-digit', day: 'numeric',
    hour: 'numeric', minute: '2-digit', second: '2-digit', hour12: true,
    timeZoneName: 'short' }
    const britishDateTime = new Intl.DateTimeFormat('en-GB', options).format
    const currentTime = britishDateTime(Date.now())
    console.log(`${currentTime}: Message received @ ${req.method} ${req.originalUrl}`) 
    //TODO add logging to file
    next()
})

router.route('/expense')
.post( async (req, res, next) => { 
    try {
        console.log(req.body.name)
        const data = await ddb.putItem({
            TableName: 'Expense',
            Item: {
                'name' : {S: req.body.name},
                'cost' : {N: req.body.cost.toString()},
                'date' : {S: new Date().toISOString()},
            }
        }).promise()
        console.log("Success : Put expense")
        res.json(data)        

    } catch(err) {
        console.log("Error", err)
        next(err)
    }
})
.delete( async(req, res, next) => {
    const data = await ddb.deleteItem({
        TableName: 'Expense',
        Key: {
            'date' : {S: req.body.id},
        }
    }).promise()
    console.log("Success : delete expense")
    res.json(data)
})

router.get('/expenses', async (req, res) => { 
    try {
        const data = await ddb.scan({
            TableName: "Expense"
        }).promise()
        console.log("Success : Get expenses")
        const resData = data.Items.map((item) => {
            return {
                'name': item.name.S, 
                'date': item.date.S,
                'cost':Number(item.cost.N)}
        })
        res.json(resData)    

    } catch(err) {
        console.log("Error", err)
        next(err)
    }
})

