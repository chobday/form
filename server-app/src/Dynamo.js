import AWS from 'aws-sdk'

//Store AWS keys according to https://docs.aws.amazon.com/sdk-for-javascript/v2/developer-guide/setting-credentials-node.html
AWS.config.update({region: 'us-east-2'})
const ddb = new AWS.DynamoDB({apiVersion: '2012-10-08'})
export default ddb