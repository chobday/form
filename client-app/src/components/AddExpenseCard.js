import React from 'react'
import Avatar from '@material-ui/core/Avatar';
import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import CardContent from '@material-ui/core/CardContent'
import AttachMoneyIcon from '@material-ui/icons/AttachMoney'
import { withStyles } from '@material-ui/core/styles';
import AddExpenseForm from './AddExpenseForm'
import PropTypes from 'prop-types';


const styles = {
  avatar: {
    backgroundColor: 'green',
  },
  card: {
    maxWidth: '400px',
    margin: 'auto',
    marginTop: '30px',
  },
  title: {
    fontSize: '1.5em',
    textAlign: 'center',
    color: 'palevioletred'
  }
}



class ExpenseCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: []
    };
  }


  render = () => {
    const {classes} = this.props
  
    return (
    <div>
      <Card className={classes.card}>
        <CardHeader
          avatar={
            <Avatar className={classes.avatar}>
              <AttachMoneyIcon/>
            </Avatar>
          }
          title='Basic Form'
          classes={{title: classes.title}}
        />
        <CardContent>
          <AddExpenseForm 
            hideScreen={this.props.hideScreen}
          />
        </CardContent>
      </Card>

    </div>
    )
  }
}

ExpenseCard.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ExpenseCard)