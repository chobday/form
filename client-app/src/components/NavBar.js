import React from 'react'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
const NavBar = (props) => {
    return(
        <div onClick={props.handleClick}>
            <AppBar position="static">
            <Toolbar>
                <Typography variant="title" color="inherit">
                Expense Tracker : Form Application
                </Typography>
            </Toolbar>
            </AppBar>
        </div>
    )
}
export default NavBar;