// Render Prop
import React from 'react'
import { Formik, Form } from 'formik'
import * as yup from 'yup'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField'
import InputAdornment from '@material-ui/core/InputAdornment'
import styled from './MaterialStyledComponents'


const FormStyled = styled(Form)({
  display: 'flex',
  flexDirection: 'column',
})

const TextFieldStyled = styled(TextField)({
  marginBottom: '10px',
})


const ButtonStyled = styled(Button)({
  marginTop: '15px',
})

class AddExpenseForm extends React.Component {

  setInitialValues = () => {
    return { cost: 10 }
  }
  
  validationSchema = yup.object().shape({
    name: yup.string().required().min(3),
    cost: yup.number().positive().required(),
    remark: yup.string().min(3)
  })

  render = (props) => (
    <div>
        <Formik
            initialValues={this.setInitialValues()}
            validationSchema={this.validationSchema}
            onSubmit={this.handleSubmit} 
        > 
        {this.renderForm}
        </Formik>
    </div>
  )

  
  renderForm = ({ values, errors,
    handleChange, handleSubmit, isSubmitting }) => {
    return (
        <FormStyled>
            <TextFieldStyled // TODO refactor text fields into component
                label='Name'
                required
                helperText={errors.name}
                error={Boolean(errors.name)}

                value={values.name}
                onChange={handleChange('name')}
            />

            <TextFieldStyled
                label='Cost'
                required
                InputProps={{ startAdornment: <InputAdornment position="start">$</InputAdornment>, }}
                helperText={errors.cost}
                error={Boolean(errors.cost)}

                value={values.cost}
                onChange={handleChange('cost')}
            />

            <TextFieldStyled
                label='Remark'
                helperText={errors && errors.remark}
                error={Boolean(errors && errors.remark)}

                value={values.remark}
                onChange={handleChange('remark')}
            />

            <ButtonStyled 
              variant="contained" color="primary" 
              type='submit' disabled={isSubmitting}>
                Submit
    {/* <Icon>send</Icon> */}
            </ButtonStyled>
        </FormStyled>
    )
  }

  handleSubmit = async (values, { setSubmitting }) => {
    const res = await fetch('http://localhost:3001/api/expense', {
      method: 'POST',
      headers: {
        "Content-Type": "application/json; charset=utf-8",
      },
      body: JSON.stringify(values)
    })
    try {
      await res.text()
      console.log('Submitted')
      this.props.hideScreen()
      
    } catch(error) {
      console.error(error)
    }
  }  


}


export default AddExpenseForm;
