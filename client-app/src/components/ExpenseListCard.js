import React from 'react'
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'
import { Table, TableHead, TableBody, TableFooter, TableRow, TableCell } from '@material-ui/core'
import { Card, CardHeader, CardContent, Paper } from '@material-ui/core'
import { IconButton, Typography, CircularProgress,  } from '@material-ui/core'
import AddCircleIcon from '@material-ui/icons/AddCircle'
import DeleteIcon from '@material-ui/icons/Delete'

const tableStyles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    row: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.background.default,
        },
    },
    progressBox: {
        display: 'flex',
        justifyContent: 'center',
    },
})

class ExpenseListCard extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            error: null,
            isLoaded: false,
            items: []
        }
    }

    async componentDidMount() {
        await this.loadItems()
    }

    render = () => {
        return (
            <CardStyled>
                <CardHeaderStyled
                    title='Expenses'
                    action={
                        <IconButton 
                            onClick={this.props.showAddScreen}
                            color='primary'>
                          <AddIconStyled fontSize='large'  />
                        </IconButton>
                    }
                />
                <CardContent>
                    {this.renderExpenseList()}
                </CardContent>
            </CardStyled>
        )
    }

    renderTableHeader = () => (
        <TableHead><TableRow>
            <CustomTableCell>
                <Typography color='inherit' variant="subtitle1" gutterBottom>
                    Expense
                </Typography>
            </CustomTableCell>
            <CustomTableCell numeric padding='dense'>
                <Typography color='inherit' variant="subtitle1" gutterBottom>
                    Cost (SGD)
                </Typography>
            </CustomTableCell>
            <CustomTableCell/>
        </TableRow></TableHead>
    )

    renderExpenseRow = (item, index) => {
        if (index >= 10) {
            return
        }
        return (
            <TableRow className={this.props.classes.row} key={item.date}>
                <CustomTableCell>
                    <Typography variant="body2" gutterBottom>
                        {item.name}
                    </Typography>
                </CustomTableCell>
                <CustomTableCell numeric padding='dense'>
                    <Typography variant="body2" gutterBottom>
                    ${item.cost}
                    </Typography>
                </CustomTableCell>
                <RightTableCell padding='none'>
                    <IconButton 
                            onClick={() => {this.handleDelete(item.date)}}
                            color='default'>
                          <DeleteIcon fontSize='small'  />
                    </IconButton>
                </RightTableCell>
            </TableRow>
        )
    }

    renderTableFooter = () => (
        <TableFooter>
            {this.state.items.length > 10 && 
                <TableRow className={this.props.classes.row} key='moreItems'>                            
                    <CustomTableCell><Typography align='center' variant="caption" gutterBottom>
                        First 10 items shown ...
                    </Typography></CustomTableCell>
                </TableRow>
            }
            <TableRow className={this.props.classes.row} key='total'>                            
                <CustomTableCell><Typography variant="subtitle1" gutterBottom>
                    Total
                </Typography></CustomTableCell>
                <CustomTableCell numeric><Typography variant="subtitle1" gutterBottom>
                    ${this.state.total}
                </Typography></CustomTableCell>
            </TableRow>
        </TableFooter>
    )

    renderExpenseList = () => {
        const { classes } = this.props
        if (!this.state.isLoaded) {
            return (
                <div className={classes.progressBox}>
                    <ProgressStyled/>
                </div>
            )
        }
        return (
            <Paper className={classes.root}>
                <Table className={classes.table}>
                    {this.renderTableHeader()}
                    <TableBody> 
                        {this.state.items.map(this.renderExpenseRow)}
                    </TableBody>
                    {this.renderTableFooter()}
                </Table>
            </Paper>
        )
    }

    
    handleDelete = async (key) => {
        console.log ('Delete item with key ' + key)
        const res = await fetch('http://localhost:3001/api/expense', {
            method: 'DELETE',
            headers: {
              "Content-Type": "application/json; charset=utf-8",
            },
            body: JSON.stringify({'id':key})
          })
          try {
            await res.text()
            await this.loadItems()
                        
          } catch(error) {
            console.error(error)
          }
    }

    async loadItems() {
        const res = await fetch('http://localhost:3001/api/expenses') //TODO get from prod server based on config
        try {
            const data = await res.json()
            const items = data.sort((a,b) => {
                if (a.date > b.date) {
                    return -1
                } else if (a.date === b.date) {
                    return 0
                } else return 1
            })
            const total = data.reduce(
                (acc, curr) => {
                    return (acc + curr.cost)
            }, 0)
            this.setState({
                isLoaded: true,
                items: items,
                total: total
            })
        } catch (error) {
            console.error(error)
            this.setState({
                isLoaded: true,
                error
            })
        }       
    }
}

ExpenseListCard.propTypes = {
    classes: PropTypes.object.isRequired,
}

export default withStyles(tableStyles)(ExpenseListCard)




const AddIconStyled = withStyles(theme => ({
    root: {
        marginTop: '20px',
    },
}))(AddCircleIcon)

const CardStyled = withStyles(theme => ({
    root: {
        maxWidth: '500px',
        margin: 'auto',
        marginTop: '30px',
    },
}))(Card)

const CardHeaderStyled = withStyles(theme => ({
    title: {
        fontSize: '1.5em',
        textAlign: 'center',
        color: 'palevioletred'
    },
    root: {
        paddingBottom: '0px',
    }
}))(CardHeader)

const ProgressStyled = withStyles(theme => ({
    root: {
        margin: theme.spacing.unit * 2,
    },
}))(CircularProgress)

const RightTableCell = withStyles(theme => ({
    body: {
        textAlign: 'right',
    },
}))(TableCell)

const CustomTableCell = withStyles(theme => ({
    head: {
        backgroundColor: theme.palette.common.black,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: '0.9em',
        textTransform: 'capitalize',
    },
}))(TableCell)