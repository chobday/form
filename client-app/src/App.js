// Render Prop
import React from 'react'
import AddExpenseCard from './components/AddExpenseCard'
import ExpenseListCard from './components/ExpenseListCard'
import NavBar from './components/NavBar';
import SimpleSnackBar from './components/SimpleSnackBar';

class App extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      showScreen : 'list',
      showSnackBar: false,
    }
  }

  toggleSnackBar = () => {
    this.setState((state) => ({
      showSnackBar : !(state.showSnackBar)
    }))
    console.log(`showSnackBar ${this.state.showSnackBar}`)
  }

  showListScreen = () => {
    this.setState({
      showScreen : 'list'
    })
    this.toggleSnackBar()   
  }

  showAddScreen = (toShow) => {
    this.setState({
      showScreen : 'add'
    })
  }

  render = (props) => (
    <div>
      <NavBar/>
      { this.state.showScreen === 'add' && 
          <AddExpenseCard 
            hideScreen={this.showListScreen}
      /> }
      { this.state.showScreen === 'list' &&
        <ExpenseListCard showAddScreen={this.showAddScreen} />
      }
      <SimpleSnackBar 
        toShow={this.state.showSnackBar} 
        hideBar={this.toggleSnackBar}
      />
    </div>
  )
}


export default App